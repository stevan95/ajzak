## Kako radi ?

Svako učitvanje stranice generiše novu pesmu.  Generisanje se realizuje uz pomoć 
[Markovljevih lanaca](https://en.wikipedia.org/wiki/Markov_chain). Markovljevi Lanci
se dosta koriste u programiranju i jako su korisni za generisanje stvari na osnovu
postojećih. Na primer, tekst, muzika, slike... Objasniću na primeru teksta kako 
rade. 

Cilj Markovljevog Lanca jeste da mu mi damo jednu reč i on nam kaže sledeću.
To radi tako što napravi jednu "tabelu" gde imamo sve reči i procente koja 
reč dolazi posle. Najbolje se vidi na primeru:

--------------------------------
reč|reč1   |rec2|rec3|....|
Da | li 80%| dođem 10%| vidim 10%|
li | želiš 60%| hoćeš 20%| možeš 20%|
...|....|...|....|
------------------------------------

U kodu ovo izgleda kao rečnik(mapa) gde su ključevi reči a vrednosti su nizovi
sledećih reči, ukoliko je šansa 20% da dodje neka reč onda se ona ponavlja 20%
puta u tom nizu. Opet najbolje se vidi na primeru. Gornja tabela prikazana u strukuturi rečnika:

```
da : ["li", "li", "li", "li", "li", "li", "li", "li", "dođem", "vidim"]
li : ["želiš", "želiš", "želiš", "želiš", "želiš", "želiš", "hoćeš","hoćeš", "možeš", "možeš"]
....
```

Ovo koristimo tako što nasumično odaberemo jednu reč iz rečnika na osnovu zadate
reči, i onda naravno imamo X% šansi da dobijemo tu i tu reč. Prvo napravimo tekst
na osnovu koga ćemo generisati nove tekstove, u ovom slučaju sam iskopirao 10-ak 
pesama Ajs Nigrutina u jedan fajl.I napisao sam funkcju koja na osnovu teskta
generiše Markovljev Lanac na gore pomenut način. Evo koda te funkcije ispod:

```python
def make_chain_v2(words) -> dict:
    chain = defaultdict(list)
    for (i, word) in enumerate(words[:-1]): # prolazimo kroz svaku reč
        chain[word].append(words[i+1]) # dodamo sledeću reč u listu
    chain = dict(chain)
    return chain
```

Ceo kod ovog generatora dostupan je [ovde](https://gitlab.com/stevan95/ajzak),
možete ga slobodno menjati, dorađivati i prerađivati. Ukoliko primetite neki
bag slobodno javite preko gitlab platoforme, ako napravite neki dodatak
isto javite i dodajte svoje ime u `CONTRIBUTORS.md` fajl. Kod je napravljen
da radi kao veb aplikacija, ali je suština u `generator.py` fajlu gde se nalaze
funkcije za generisanje Markovljevog Lanca, stiha, strofe i pesme. Jako lako 
se izmeni da generiše nešto drugo, samo promenite `pesme.txt` fajl ili izmenite
kod da čita sa drugog mesta.

## Dodatni materijali

Za kraj evo još malo materijala koji mogu da pomognu u izučavanju Markovljevih 
Lanaca i obrade teksta.

- [Introduction to Markov Chains](https://towardsdatascience.com/introduction-to-markov-chains-50da3645a50d)
- [Natural Language Processing in Python](https://www.youtube.com/watch?v=xvqsFTUsOmc)
- [Coding Challenge #42.1: Markov Chains - Part 1](https://www.youtube.com/watch?v=eGFJ8vugIWA)

