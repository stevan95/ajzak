"""
Generator Pesama Ajs Nigrutina
    Copyright (C) 2020  Stevan Nestorvić

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""

import json
import random
from collections import defaultdict

def get_song() -> str:
    with open('/home/user/ajzak/ajzak/pesme.txt', 'r') as f:
        text = f.read()
    text = clean_text(text)
    words = text.split(' ')
    chain = make_chain(words)
    song = make_song(chain=chain, count=4)
    return song

def get_title() -> str:
    with open('/home/user/ajzak/ajzak/naslovi.txt', 'r') as f:
        text = f.read()
    text = clean_text(text)
    words = text.split(' ')
    #chain = make_chain(words)
    chain = make_chain_v2(words)
    title_length = random.randint(1,4)
    title =  make_sentence(chain=chain, count=title_length)
    print(title)
    return title


def make_verse(chain, count=4) -> str:
    verses = []
    for _ in range(count):
        verses.append(make_sentence(chain))
    return '\n'.join(verses)

def make_song(chain, count=4) -> str:
    strofe = []
    for _ in range(count):
        strofe.append(make_verse(chain=chain, count=4))
    song = '\n\n'.join(strofe) 
    return song

def make_chain(words) -> dict:
    chain = defaultdict(list)
    for current_word, next_word in zip(words[0:-1], words[1:]):
        chain[current_word].append(next_word)
    chain = dict(chain)
    return chain

def make_chain_v2(words) -> dict:
    chain = defaultdict(list)
    for (i, word) in enumerate(words[:-1]):
        chain[word].append(words[i+1])
    chain = dict(chain)
    return chain

def make_sentence(chain:dict, count:int=8) -> str:
    w1 = random.choice(list(chain.keys()))
    sentence = w1.capitalize()
    for i in range(count - 1):
        w2 = random.choice(chain[w1])
        w1 = w2
        sentence += ' ' + w2
    return sentence

def clean_text(text:str='') -> str:
    symbols = ('.', ',', ':', '"', '\'')
    for sym in symbols:
	    text = text.replace(sym,'')
    return text.replace('\n', ' ')

if __name__ == '__main__':
    main()
