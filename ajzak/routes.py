"""
Generator Pesama Ajs Nigrutina
    Copyright (C) 2020  Stevan Nestorvić

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
from ajzak import app, generator
from flask import render_template


@app.route('/')
@app.route('/index')
def index():
    song = generator.get_song()
    title = generator.get_title()
    data = {'song':song, 'title':title}
    return render_template('pages/index.html', data=data)

@app.route('/kako-radi')
def how():
    return render_template('pages/kako-radi.html')
	
